/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.EmployeeRecords.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author yavor
 */
@Entity
@Table(name = "Employees")
public class Employee implements Serializable {
    
    @Id
    @Column(name = "firstName", length = 20, nullable = false)
    private String firstName;
    @Column(name = "lastName", length = 30, nullable = false)
    private String lastName;
    @Column(name = "Address", length = 50, nullable = true)
    private String address;
    @Column(name = "Position", length = 20, nullable =  true)
    private String position;
    @Column(name = "Country", length = 20, nullable = true)
    private String country;
    @Column(name = "Picture", length = Integer.MAX_VALUE, nullable = true)
    private byte[] image;
    
    public Employee() {}
    
    public Employee(byte[] image, String firstName, String lastName, String address, String position, String country) {
        
        this.image = image;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.position = position;
        this.country = country;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    
}
