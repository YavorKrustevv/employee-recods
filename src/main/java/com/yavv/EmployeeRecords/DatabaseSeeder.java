package com.yavv.EmployeeRecords;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.yavv.EmployeeRecords.model.Employee;
import com.yavv.EmployeeRecords.repository.EmployeeRepository;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 * @author yavor
 */
@Component
public class DatabaseSeeder implements CommandLineRunner {

    /**
     * Contains all employees
     */
    public List<Employee> employees;

    @Autowired
    private EmployeeRepository categoryRepository;

    public DatabaseSeeder() {
        this.employees = new ArrayList<>();
    }

    File pic1Path = new File("test0.png");
    File pic2Path = new File("test1.png");
    /*
    public byte[] picToByte(String imageName) {
        BufferedImage bufferedImage = ImageIO.read(imageName);
    }

    @Override
    public void run(String... args) throws Exception {

        employees.add(new Employee("Yavor", "Krustev", "Svisthov", "developer", "Bulgaria"));
        employees.add(new Employee("Ivan", "Ivanov", "Ivanovo", "developer", "Bulgaria"));

        categoryRepository.saveAll(employees);

    }
*/
}
