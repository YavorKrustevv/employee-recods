/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.EmployeeRecords.controller;

import com.yavv.EmployeeRecords.DatabaseSeeder;
import com.yavv.EmployeeRecords.form.EmployeeForm;
import com.yavv.EmployeeRecords.model.Employee;
//import java.util.ArrayList;
//import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author yavor
 */
@Controller
public class MainController {
    
    
    @Autowired
    private DatabaseSeeder databaseSeeder;
    //private static List<Employee> employees = new ArrayList<Employee>();
    /*
    static {
        employees.add(new Employee("Yavor", "Krustev", "Svisthov", "developer", "Bulgaria"));
        employees.add(new Employee("Ivan", "Ivanov", "Ivanovo", "developer", "Bulgaria"));
    }
    */
    
    //Inject via application.properties
    @Value("${welcome.message}")
    private String message;

    @Value("${error.message}")
    private String errorMessage;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {

        model.addAttribute("message", message);

        return "index";
    }

    @RequestMapping(value = {"/employeeList"}, method = RequestMethod.GET)
    public String employeeList(Model model) {

        model.addAttribute("employees", databaseSeeder.employees);

        return "employeeList";
    }

    // Both methods GET and POST on same url.
    @RequestMapping(value = {"/addEmployee"}, method = RequestMethod.GET)
    public String showAddEmployeePage(Model model) {

        EmployeeForm employeeForm = new EmployeeForm();
        model.addAttribute("employeeForm", employeeForm);

        return "addEmployee";
    }

    @RequestMapping(value = {"/addEmployee"}, method = RequestMethod.POST)
    public String saveEmployee(Model model, @ModelAttribute("employeeForm") EmployeeForm employeeForm) {
        
        byte[] picture = employeeForm.getPicture();
        String firstName = employeeForm.getFirstName();
        String lastName = employeeForm.getLastName();
        String address = employeeForm.getAddress();
        String position = employeeForm.getPosition();
        String country = employeeForm.getCountry();

        if (firstName != null && firstName.length() > 0
                && lastName != null && lastName.length() > 0) {

            Employee newEmployee = new Employee(picture, firstName, lastName, address, position, country);
            databaseSeeder.employees.add(newEmployee);

            return "redirect:/employeeList";
        }
        model.addAttribute("errorMessage", errorMessage);
        return "addEmployee";
    }

}
