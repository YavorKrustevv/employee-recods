/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yavv.EmployeeRecords.repository;

import com.yavv.EmployeeRecords.model.Employee;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author yavor
 */
public interface EmployeeRepository extends CrudRepository<Employee, String> {
    
    
}
